package com.deepan.service;

public class PaypalPaymentGateway {

    private String description;

    public PaypalPaymentGateway(){
        this.description = "sample Paypal Gateway";
    }

    @Override
    public String toString() {
        return "PaypalPaymentGateway{" +
                "description='" + description + '\'' +
                '}';
    }
}
