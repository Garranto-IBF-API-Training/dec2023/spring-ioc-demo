package com.deepan.model;

import org.springframework.stereotype.Component;

@Component
public class Account {

    private String accountNo;
    private double balance;

    private String type;


    public Account(String accountNo, double balance, String type) {
        this.accountNo = accountNo;
        this.balance = balance;
        this.type = type;
    }

    public Account(){}

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountNo='" + accountNo + '\'' +
                ", balance=" + balance +
                ", type='" + type + '\'' +
                '}';
    }
}
