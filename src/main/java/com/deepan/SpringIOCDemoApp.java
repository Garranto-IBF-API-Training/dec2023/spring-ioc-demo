package com.deepan;

import com.deepan.model.Account;
import com.deepan.service.PaypalPaymentGateway;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringIOCDemoApp {
    public static void main(String[] args) {
        System.out.println("Spring IOC Container Demo");
        System.out.println("==============================");

//        factory object ( object that creates and manages beans configured in the config metadata)
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("beans.xml");

        Account account = applicationContext.getBean("accountOne",Account.class);
        PaypalPaymentGateway gateway = applicationContext.getBean("paypalGateway", PaypalPaymentGateway.class);
        System.out.println(account);
        System.out.println(gateway);
    }
}

//spring core library -  spring context

//model class -- defines the structure of the data
//Account -- what are the fields and access functions --  plain old java object
//service classes
//controller classes
//config classes  -- factory classes

//  Account accountOne = new Account("AC009",1290.12,"Savings");
//        Account accountTwo = new Account("AC00101",4500.12,"Current");
//        accountOne.setBalance(34999.99);
//        System.out.println(accountOne.getBalance());
////        System.out.println(accountOne);
////        System.out.println(accountTwo);

//xml config
//java
//annotation
